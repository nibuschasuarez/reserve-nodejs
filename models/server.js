const express = require ('express');

class Server{

  constructor(){
      this.app = express();
      this.port = process.env.PORT;
      this.usariosPath = '/api/usuarios';

      this.middleware();
      this.routes();
  }

  middleware(){ 
    //Lectura y parseo del body
    //Cualquier informacion que venga se serializara en formato JSON
    this.app.use(express.json());



    //se publica la carpeta public
    this.app.use(express.static('public'))
  }

  routes() {
  
      this.app.get('/api', (req, res) => {
        res.json({
          msg: 'get API'
        });
      });
  
      this.app.put('/api', (req, res) => {
        res.json({
          msg: 'put API'
        });
      });
  
      this.app.post('/api', (req, res) => {
        res.status(201).json({
          msg: 'post API'
        });
      }); 
      this.app.delete('/api', (req, res) => {
        res.json({
          msg: 'delete API'
        });
      });
  
    } 
    routes() {

      this.app.use( this.usariosPath,require('../routes/user'));
    }
  

    listen() {
         //Con process.env.PORT obtenemos el valor de la variable PORT que esta en el archivo .env
         this.app.listen(this.port, () => {
          console.log('Servidor corriendo en el puerto', this.port);
        });
    }
 
  }
 
     



module.exports= Server;