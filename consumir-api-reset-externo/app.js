const express = require('express');
const axios = require('axios');
const res = require('express/lib/response');
const app = express();

const port = 8080; 

const url = 'https://jsonplaceholder.typicode.com/users';

app.get('/', (req, res) => {
  res.send('Hello World');
});

app.get('/api/users', async(req, res) =>{
  try {
    const jsonResponse = await axios.get(url);
    const response = jsonResponse.data;
    res.status(200).json({
      response
    })
  } catch (error){ 
    console.log(error);

  }
})

app.listen(port, () => {
  console.log(`servidor corriendo en el puerto ${port}`);
});
