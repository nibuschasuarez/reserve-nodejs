const { Router } = require('express');
const { usuarioPut, usuarioPost, usuarioDelete, usuarioGet } = require('../controllers/user');

const router = Router();

router.get('/', usuarioGet);

router.put('/:id', usuarioPut);

router.post('/', usuarioPost);

router.delete('/', usuarioDelete);




module.exports = router;